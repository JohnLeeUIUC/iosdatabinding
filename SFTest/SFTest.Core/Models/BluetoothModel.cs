﻿using System;
using System.Collections.Generic;
using System.Text;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SFTest.Core.Models
{
	public class BleInformation
	{
		public static readonly Collection<string> MasterCharacteristicUuids = new Collection<string> { "2a37", "2a18", "2a1c" };
		public static readonly Collection<string> SlaveCharacteristicUuids = new Collection<string> { "2a49", "2a1c" };
		public static Dictionary<DataType, uint> DataPoints = new Dictionary<DataType, uint> { { DataType.ecg, 500 }, { DataType.scg, 100 }, { DataType.foot_temp, 10 }, { DataType.chest_temp, 10 }, { DataType.ir, 100 }, { DataType.red, 100 } };
		public static Dictionary<DataType, uint> SamplingRate = new Dictionary<DataType, uint>() { { DataType.ecg, 5 }, { DataType.scg, 1 }, { DataType.foot_temp, 1 }, { DataType.chest_temp, 1 }, { DataType.ir, 1 }, { DataType.red, 1 } };
	}

	public enum DataType { None, ecg, scg, red, temp, foot_temp, chest_temp, ppg, ir, accl }
	public enum DeviceType { None, Master, Slave };
	public class DataProfile
	{
		public DataType _type;
		public UInt32 _index;
		public double _value;

        public double Value { get { return _value; } }
		public DataType Type { get { return _type; } }
		public UInt32 Index { get { return _index; } }
	}

	public class DeviceProfile
	{
		public static readonly Dictionary<DeviceType, Collection<DataType>> DataTypesAvailable = new Dictionary<DeviceType, Collection<DataType>>() {
				{ DeviceType.Master, new Collection<DataType> {DataType.ecg, DataType.scg, DataType.chest_temp }},
				{ DeviceType.Slave, new Collection<DataType> { DataType.ir, DataType.red, DataType.foot_temp }}
			};
		public static readonly Collection<DataType> DataTypesOnNewSession = new Collection<DataType>() {
				DataType.ecg, DataType.accl, DataType.ppg, DataType.chest_temp, DataType.foot_temp
			};
		public String DeviceName { get; set; }
		public String DeviceId { get; set; }
		public String Battery { get; set; }
		public String DeviceImage { get; set; }
		public ulong Address { get; set; }
	}

	public class BleDataReceived : DataProfile
	{
		public BleDataReceived(DataType type, UInt32 idx, double data)
		{
			_type = type;
			_index = idx;
			_value = data;
		}
	}
}

