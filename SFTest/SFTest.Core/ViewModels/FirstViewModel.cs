using System.Collections.ObjectModel;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using Plugin.BLE.Abstractions.Contracts;
using System.Collections.Concurrent;
using Acr.UserDialogs;
using Plugin.BLE.Abstractions.EventArgs;
using SFTest.Core.Models;

namespace SFTest.Core.ViewModels {
    public class FirstViewModel
        : MvxViewModel {
        private IAdapter _adapter;
        private IBluetoothLE _ble;
        private static bool _emptyDevices { get; set; } = false;
        public static bool EmptyDevices { get { UiUpdateRequested.Invoke(); return _emptyDevices; } set { _emptyDevices = value; UiUpdateRequested.Invoke(); } }

        public delegate void ToggleScanRequestReceivedHandler();
        public static event ToggleScanRequestReceivedHandler ToggleScanRequested;
        public delegate void ConnectDeviceReceivedHandler(DeviceProfile device);
        public static event ConnectDeviceReceivedHandler DeviceConnectedTrigger;
        public delegate void DisconnectDeviceReceivedHandler(DeviceProfile device);
        public static event DisconnectDeviceReceivedHandler DeviceDisconnectedTrigger;
        public delegate void DeviceDisconnectedNotificationReceivedHandler();
        public static event DeviceDisconnectedNotificationReceivedHandler DeviceDisconnectedNotifed;
        public delegate void UserInterfaceUpdateHandler();
        public static event UserInterfaceUpdateHandler UiUpdateRequested;

        Thread DataWriter1;
        Thread DataWriter2;
        private Queue<BleDataReceived> queue1 = new Queue<BleDataReceived>();
        private Queue<BleDataReceived> queue2 = new Queue<BleDataReceived>();

        public ObservableDictionary<string, MvxObservableCollection<BleDataReceived>> DataListString { get; set; } = new ObservableDictionary<string, MvxObservableCollection<BleDataReceived>>();
        public ConcurrentDictionary<DataType, MvxObservableCollection<BleDataReceived>> DataList { get; set; } = new ConcurrentDictionary<DataType, MvxObservableCollection<BleDataReceived>>();
        public ConcurrentDictionary<DataType, UInt32> DataTime { get; set; } = new ConcurrentDictionary<DataType, UInt32>();
        public ConcurrentDictionary<DataType, MvxObservableCollection<BleDataReceived>> DataListMobile { get; set; } = new ConcurrentDictionary<DataType, MvxObservableCollection<BleDataReceived>>();
        public MvxObservableCollection<IDevice> ScannedDevices { get; set; } = new MvxObservableCollection<IDevice>();

        public static string MasterTempMin { get; set; } = "-";
        public static string MasterTempMax { get; set; } = "-";
        public static string SlaveTempMin { get; set; } = "-";
        public static string SlaveTempMax { get; set; } = "-";
        public static string SlaveTemp { get; set; } = "-";
        public static string MasterTemp { get; set; } = "-";
        private string _masterTemp { get; set; } = "-";
        private string _slaveTemp { get; set; } = "-";

        private int a = 0;

        public ObservableCollection<Model> Data { get; set; }
        public ObservableCollection<Model> Data2 { get; set; }
        public ObservableDictionary<string, MvxObservableCollection<BleDataReceived>> Data3 { get; set; } = new ObservableDictionary<string, MvxObservableCollection<BleDataReceived>>();
        public FirstViewModel(IAdapter adapter, IBluetoothLE bluetoothLe) {
            Data = new ObservableCollection<Model>();
            Data2 = new ObservableCollection<Model>();
            Data.Add(new Model(a++, a++));
            Data.Add(new Model(a++, a++));
            Data.Add(new Model(a++, a++));
            Data.Add(new Model(a++, a++));
            Data.Add(new Model(a++, a++));
            Data2.Add(new Model(1, 5));
            Data2.Add(new Model(2, 4));
            Data2.Add(new Model(3, 3));
            Data2.Add(new Model(5, 1));
            Data2.Add(new Model(4, 2));
            Data3.Add("DataType.ecg", new MvxObservableCollection<BleDataReceived>());

            for (uint x = 0; x < 1000; x++) {
                Data3["DataType.ecg"].Add(new BleDataReceived(DataType.ecg, x, Double.NaN));
            }

            for (uint x = 0; x < 1000; x++) {
                Data3["DataType.ecg"].Add(new BleDataReceived(DataType.ecg, x, x + 1000));
            }
            for (uint x = 0; x < 1000; x++) {
                Data3["DataType.ecg"].Add(new BleDataReceived(DataType.ecg, x+1000, x + 1000));
            }
            SetMobileBluetooth(adapter, bluetoothLe);

        }

        void SetMobileBluetooth(IAdapter adapter, IBluetoothLE bluetoothLe) {
            _adapter = adapter;
            _ble = bluetoothLe;

            ConnectedDevices.TryAdd(DeviceType.Master, null);
            ConnectedDevices.TryAdd(DeviceType.Slave, null);

            foreach (DataType type in Enum.GetValues(typeof(DataType))) {
                try {
                    //TODO[UHMS - 84]: DataList.Add(type, new MvxObservableCollection<DataProfile>());
                    DataListString.Add("DataType." + type.ToString(), new MvxObservableCollection<BleDataReceived>());
                    DataList.TryAdd(type, new MvxObservableCollection<BleDataReceived>());
                    DataListMobile.TryAdd(type, new MvxObservableCollection<BleDataReceived>());

                    DataTime.TryAdd(type, 0);
                    //DataListString["DataType." + type.ToString()] = DataListMobile[type];
                    for (uint i = 0; i < BleInformation.DataPoints[type] * BleInformation.SamplingRate[type]; i++) {
                        DataList[type].Add(new BleDataReceived(type, i, Double.NaN));
                        DataListMobile[type].Add(new BleDataReceived(type, i, Double.NaN));
                    }
                    Debug.WriteLine(DataListString["DataType.ecg"].Count);

                    Debug.WriteLine("length: " + DataList[type].Count + "," + DataListString["DataType." + type.ToString()].Count);
                    Debug.WriteLine("DataType." + type.ToString());
                } catch (Exception ex) {
                    Debug.WriteLine("Exception :" + ex.Message);
                }
                RaiseAllPropertiesChanged();
            }
            DataWriter1 = new Thread(() => PopAndWrite(queue1));
            DataWriter2 = new Thread(() => PopAndWrite(queue2));
            DataWriter1.Start(); DataWriter2.Start();

            //SetBluetooth();
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 0, .1));
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 1, .1));
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 2, .1));
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 3, .1));
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 4, .1));
            DataListMobile[DataType.ecg].Add(new BleDataReceived(DataType.ecg, 5, .1));

            _adapter.DeviceDiscovered += (s, d) => {

                if (d.Device.Name == "nicu chest" || d.Device.Name == "nicu foot") {
                    AttemptConnectOrDispose(d.Device);
                }
            };
        }

        public MvxCommand ScanRequested => new MvxCommand(() => {
            StartScanning();
            //RaiseAllPropertiesChanged();
        });

        public MvxCommand<IDevice> ConnectOrDispose => new MvxCommand<IDevice>((device) => {
            if (_adapter.ConnectedDevices.Contains(device))
                AttemptConnectOrDispose(device, false);
            else AttemptConnectOrDispose(device);
        });

        private void AddToListIfNotNull(DeviceEventArgs d) {
            if (d.Device.Name != "" || d.Device.Name != null)
                ScannedDevices.Add(d.Device);

        }

        private async void StartScanning() {
            for (uint x = 0; x < 1000; x++) {
                Data3["DataType.ecg"].Add(new BleDataReceived(DataType.ecg, 3000 + x, 2000));
            }
            if (!_ble.IsAvailable) {
                UserDialogs.Instance.Alert("Check your device. Ble is not available.");
                return;
            } else if (!_ble.IsOn) {
                UserDialogs.Instance.Alert("Turn on your bluetooth.");
                return;
            }

            await _adapter.StopScanningForDevicesAsync();
            ScannedDevices.Clear();
            await _adapter.StartScanningForDevicesAsync();
        }

        public ConcurrentDictionary<DeviceType, IDevice> ConnectedDevices = new ConcurrentDictionary<DeviceType, IDevice>();

        public MvxObservableCollection<IDevice> GetConnectedDevices {
            get {
                return new MvxObservableCollection<IDevice>(ConnectedDevices.Values);
            }
        }

        private DataType CharacteristicToDataTypeTranslator(ICharacteristic characteristic) {
            //2a18 = GlucoseMeasurement
            //2a37 = HeartRateMeasurement
            //2a1c = TemperatureMeasurement
            //2a49 = BloodPressureFeature
            if (characteristic.Uuid == "2a18")
                return DataType.ecg;
            if (characteristic.Uuid == "2a37")
                return DataType.scg;
            if (characteristic.Uuid == "2a1c")
                return DataType.temp;
            if (characteristic.Uuid == "2a49")
                return DataType.ppg;
            else return DataType.None;
        }

        private async void AttemptConnectOrDispose(IDevice device, bool connection = true) {
            if (!_ble.IsAvailable)
                UserDialogs.Instance.Alert("Check your device. Ble is not available.");
            else if (!_ble.IsOn)
                UserDialogs.Instance.Alert("Turn on your bluetooth.");

            if (device == null)
                return;
            var connectionStatus = connection;

            if (connectionStatus) {
                //if should connect.
                try {
                    var cancelSrc = new CancellationTokenSource();
                    var config = new ProgressDialogConfig()
                        .SetTitle("Trying to establish connection with device...")
                        .SetIsDeterministic(false)
                        .SetMaskType(MaskType.None)
                        .SetCancel(onCancel: cancelSrc.Cancel);
                    using (UserDialogs.Instance.Progress(config)) {
                        await Task.Run(async () => { await ConnectToDevice(device); }, cancelSrc.Token);
                        if (!cancelSrc.IsCancellationRequested)
                            UserDialogs.Instance.Alert("Connected to Device", "Success", "Ok");
                    }
                } catch (Exception e) {
                    UserDialogs.Instance.Alert(new AlertConfig {
                        Title = "Error Occurred",
                        Message = "If this event occurred, please notify developers with following message.\n" + e.Message + "."
                    });
                }
            } else {
                //cases when it should disconnect
                try {
                    await _adapter.DisconnectDeviceAsync(device);
                    Debug.WriteLine(device.Name + " has been disconnected.");
                    UserDialogs.Instance.Alert(new AlertConfig {
                        Title = "Successful",
                        Message = "Successfully disconnected."
                    });
                } catch (Exception e) {
                    UserDialogs.Instance.Alert(new AlertConfig {
                        Title = "Error Occurred",
                        Message = "If this event occurred, please notify developers with following message.\n" + e.Message + "."
                    });
                }
            }
        }

        private async Task<bool> ConnectToDevice(IDevice device) {
            int i = 0;
            await _adapter.ConnectToDeviceAsync(device);
            ConcurrentDictionary<DataType, ICharacteristic> discoveredCharacteristics = new ConcurrentDictionary<DataType, ICharacteristic>();
            var services = await device.GetServicesAsync();
            foreach (var service in services) {
                var characteristics = await service.GetCharacteristicsAsync();
                foreach (var characteristic in characteristics) {
                    if (characteristic.CanUpdate) {
                        Debug.WriteLine(characteristic.Name + " " + characteristic.Uuid);
                        discoveredCharacteristics.TryAdd(CharacteristicToDataTypeTranslator(characteristic), characteristic);
                    }
                }
            }

            DeviceType deviceType = DeviceType.None;
            Collection<string> deviceCharacteristicUuids = new Collection<string>();
            foreach (var characteristic in discoveredCharacteristics.Values) {
                deviceCharacteristicUuids.Add(characteristic.Uuid);
            }
            bool isMaster = BleInformation.MasterCharacteristicUuids.All(s => deviceCharacteristicUuids.Contains(s));
            bool isSlave = BleInformation.SlaveCharacteristicUuids.All(s => deviceCharacteristicUuids.Contains(s));
            if (isMaster || isSlave) {
                if (isMaster)
                    deviceType = DeviceType.Master;
                else if (isSlave)
                    deviceType = DeviceType.Slave;
            }

            Debug.WriteLine("\nPrevious Master Address: " + ConnectedDevices[DeviceType.Master]?.Id);
            Debug.WriteLine("Previous Slave Address: " + ConnectedDevices[DeviceType.Slave]?.Id);
            Debug.WriteLine("Device Type: " + deviceType);

            string type = deviceType == DeviceType.Master ? "chest" : "foot";
            bool response = true;
            if (ConnectedDevices[deviceType] != null) {
                response = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig {
                    Message = "There is already " + type + "device connected.",
                    Title = "Device conflict",
                    OkText = "Yes, connect this device.",
                    CancelText = "No, use previous device connected."
                });
            }
            if (response) {
                if (ConnectedDevices[deviceType] != null)
                    await DisconnectDevice(ConnectedDevices[deviceType]);
                try {
                    bool deviceExists = false;
                    if (ConnectedDevices[DeviceType.Master] != null || ConnectedDevices[DeviceType.Slave] != null)
                        deviceExists = true;
                    //await FileManagementModel.CreateNewLogFile(deviceExists);
                    ConnectedDevices[deviceType] = device;

                    Debug.WriteLine("New Master Address: " + ConnectedDevices[DeviceType.Master]?.Id);
                    Debug.WriteLine("New Slave Address: " + ConnectedDevices[DeviceType.Slave]?.Id + "\n");
                    foreach (var characteristic in discoveredCharacteristics.Values) {
                        await characteristic.StartUpdatesAsync();
                        characteristic.ValueUpdated += (o, args) => DataUpdate(args.Characteristic.Value, CharacteristicToDataTypeTranslator(characteristic), deviceType);
                    }
                } catch (Exception e) {
                    Debug.WriteLine(e.Message);
                }
            }

            return true;
        }

        private async Task<bool> DisconnectDevice(IDevice device) {
            await _adapter.DisconnectDeviceAsync(device);
            return true;
        }

        public void DataUpdate(byte[] data, DataType type, DeviceType senderDeviceType) {
            List<MvxObservableCollection<BleDataReceived>> dataCollection = new List<MvxObservableCollection<BleDataReceived>>();
            dataCollection.Add(new MvxObservableCollection<BleDataReceived>());
            if (senderDeviceType == DeviceType.Master) {
                if (type == DataType.temp) {
                    var raw = data[0] << 8 | data[1];
                    var temp = raw * 0.00390625;
                    temp = (int)(temp * 10);
                    temp = temp * .1;
                    MasterTemp = temp.ToString();
                    Debug.WriteLine("Master Temperature" + MasterTemp + "\t Time: " + DateTime.Now);
                    UiUpdateRequested?.Invoke();
                    dataCollection[0].Add(new BleDataReceived(DataType.chest_temp, DataTime[DataType.chest_temp]++, temp));
                    //DataTime[DataType.masterTemp]++; DataTime[DataType.slaveTemp]++;
                } else if (type == DataType.scg) {
                    for (int i = 0; i < 3; i++) {
                        dataCollection[0].Add(new BleDataReceived(type, DataTime[type]++, (UInt16)(data[6 * i + 1] << 8 | data[6 * i])));
                        dataCollection[0].Add(new BleDataReceived(type, DataTime[type]++, (UInt16)(data[6 * i + 3] << 8 | data[6 * i + 2])));
                        dataCollection[0].Add(new BleDataReceived(type, DataTime[type]++, (UInt16)(data[6 * i + 5] << 8 | data[6 * i + 4])));
                    }
                } else if (type == DataType.ecg) {
                    for (int i = 0; i < 10; i++)
                        dataCollection[0].Add(new BleDataReceived(type, DataTime[type]++, (UInt16)(data[2 * i + 1] | data[2 * i] << 8)));
                } else {
                    Debug.WriteLine("data leak");
                }
            } else if (senderDeviceType == DeviceType.Slave) {
                if (type == DataType.temp) {
                    var raw = data[0] << 8 | data[1];
                    var temp = raw * 0.00390625;
                    temp = (int)(temp * 10);
                    temp = temp * .1;
                    SlaveTemp = temp.ToString();
                    Debug.WriteLine("Slave Temperature" + SlaveTemp + "\t Time: " + DateTime.Now);
                    UiUpdateRequested?.Invoke();
                    dataCollection[0].Add(new BleDataReceived(DataType.foot_temp, DataTime[DataType.foot_temp]++, (UInt16)temp));
                    //DataTime[DataType.masterTemp]++; DataTime[DataType.slaveTemp]++;
                } else if (type == DataType.ppg) {
                    dataCollection.Add(new MvxObservableCollection<BleDataReceived>());
                    for (int i = 0; i < 5; i++) {
                        var reddata = new BleDataReceived(DataType.red, DataTime[DataType.red]++, (UInt16)((data[4 * i + 1] << 8) | data[4 * i]));
                        var irdata = new BleDataReceived(DataType.ir, DataTime[DataType.ir]++, (UInt16)((data[4 * i + 3] << 8) | data[4 * i + 2]));
                        dataCollection[0].Add(reddata);
                        dataCollection[1].Add(irdata);
                    }
                } else {
                    Debug.WriteLine("data leak");
                }
            } else {
                Debug.WriteLine("Data Leak: " + type);
            }

            //static int Maximum = 500;
            //Update user interface.
            //MvxObservableCollection<BleDataReceived> newDataCollection = new MvxObservableCollection<BleDataReceived>();
            foreach (var collection in dataCollection) {
                if (collection.Count > 0) {
                    MvxObservableCollection<BleDataReceived> newDataCollection = null;
                    DataType specificType = DataType.None;
                    foreach (var dat in collection) {
                        if (newDataCollection == null) {
                            newDataCollection = new MvxObservableCollection<BleDataReceived>(DataListString["DataType." + type.ToString()]);
                            specificType = dat.Type;
                        }
                        if ((dat.Index % BleInformation.SamplingRate[dat.Type] == 0)) {
                            if (DataList[dat.Type].Count > BleInformation.DataPoints[dat.Type]) {
                                for (int i = 1; i < BleInformation.SamplingRate[dat.Type]; i++) {
                                    //newDataCollection.RemoveAt(0);
                                }
                            }
                            //newDataCollection.RemoveAt(0);
                            newDataCollection.Add(dat);
                            RaisePropertyChanged("DataListString");
                        }
                    }
                    DataListString["DataType."+ specificType.ToString()] = newDataCollection;
                } else {
                    Debug.WriteLine("UI: " + type);
                }
            }
            Dispatcher.RequestMainThreadAction(() => {
                foreach (var collection in dataCollection)
                    EnqueueData(collection);
            });
        }

        public void EnqueueData(Collection<BleDataReceived> data) {
            if (!DataWriter1.IsAlive) {
                foreach (var d in data)
                    queue1.Enqueue(d);
                if (!DataWriter2.IsAlive) {
                    DataWriter1 = new Thread(() => PopAndWrite(queue1)) { IsBackground = true };
                    DataWriter1.Start();
                }
            } else if (!DataWriter2.IsAlive) {
                foreach (var d in data)
                    queue2.Enqueue(d);
                if (!DataWriter1.IsAlive) {
                    DataWriter2 = new Thread(() => PopAndWrite(queue2)) { IsBackground = true };
                    DataWriter2.Start();
                }
            } else {
                Debug.WriteLine("DATA IS LEAKING");
            }
        }

        public void PopAndWrite(Queue<BleDataReceived> q) {
            while (q.Count != 0) {
                BleDataReceived data = q.Dequeue();
                //FileManagementModel.WriteToFile(data).Wait();
            }
        }



        string hello = "Hello MvvmCross";
        public string Hello {
            get { return hello; }
            set { SetProperty(ref hello, value); }
        }

        public class Model {
            public double xData { get; set; }
            public double yData { get; set; }

            public Model(double dateTime, double value) {
                xData = dateTime;
                yData = value;
            }
        }
    }
}
