﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using SFTest.Core.Models;
using SFTest.Core.ViewModels;
using Syncfusion.SfChart.iOS;
using UIKit;

namespace SFTest.iOS.Views {
    [MvxFromStoryboard]
    public partial class FirstView : MvxViewController {
        public FirstView(IntPtr handle) : base(handle) {
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();
            var set = this.CreateBindingSet<FirstView, FirstViewModel>();

            //Adding Primary Axis for the Chart.
            SFNumericalAxis primaryAxis = new SFNumericalAxis() {
                //Maximum = new NSNumber(3000),
                //Maximum = new NSNumber(10),

                //Minimum = new NSNumber(0)
            };
            RealTimeGraph.PrimaryAxis = primaryAxis;
            //primaryAxis.Minimum = new NSNumber(0);
            //primaryAxis.Maximum = new NSNumber(2000);

            //Adding Secondary Axis for the Chart.
            SFNumericalAxis secondaryAxis = new SFNumericalAxis() {
                //Maximum = new NSNumber(3000),
                //Maximum = new NSNumber(10),
                //Minimum = new NSNumber(0)
            };
            //secondaryAxis.Maximum = new NSNumber(4000);
            //secondaryAxis.Minimum = new NSNumber(0);
            RealTimeGraph.SecondaryAxis = secondaryAxis;
            Collection<SFFastLineSeries> sFFastLineSeries = new Collection<SFFastLineSeries>();

            sFFastLineSeries.Add(new SFFastLineSeries() {
                XBindingPath = "Index",
                YBindingPath = "Value",
                LineWidth = 1
            });

            //Initialize the Chart with required frame. This frame can be any rectangle, which bounds inside the view.
            for (int i = 0; i < 20; i++) {
                sFFastLineSeries.Add(new SFFastLineSeries() {
                    XBindingPath = "Index",
                    YBindingPath = "Value",
                    LineWidth = 1,
                });
            }
            //RealTimeGraph.Series.Add(fastLineSeries);
            for (int i = 0; i < sFFastLineSeries.Count; i++)
                RealTimeGraph.Series.Add(sFFastLineSeries[i]);

            //SFLineSeries graph2Data = new SFLineSeries();
            //RealTimeGraph2.Series.Add(graph2Data);

            //set.Bind(Label).To(vm => vm.Hello);
            //set.Bind(TextField).To(vm => vm.Hello);

            set.Bind(StartScanning)
                .To(vm => vm.ScanRequested);

            set.Bind(sFFastLineSeries[0])
               .To(vm => vm.Data3["DataType.ecg"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[0].Color = UIColor.Blue;

            set.Bind(sFFastLineSeries[1])
               .To(vm => vm.DataListString["DataType.ecg"])
               //.To(vm => vm.DataList[DataType.ecg])
               .For(v => v.ItemsSource);
            sFFastLineSeries[1].Color = UIColor.Yellow;

            set.Bind(sFFastLineSeries[2])
               .To(vm => vm.DataListString["DataType.red"])
               //.To(vm => vm.DataList[DataType.red])
               .For(v => v.ItemsSource);
            sFFastLineSeries[2].Color = UIColor.Red;

            set.Bind(sFFastLineSeries[3])
               .To(vm => vm.DataListString["DataType.ir"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[3].Color = UIColor.Purple;

            set.Bind(sFFastLineSeries[4])
               .To(vm => vm.DataListString["DataType.foot_temp"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[4].Color = UIColor.Orange;

            set.Bind(sFFastLineSeries[5])
               .To(vm => vm.DataListString["DataType.chest_temp"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[5].Color = UIColor.Green;

            set.Bind(sFFastLineSeries[6])
               .To(vm => vm.DataListString["DataType.chest_temp"])
               .For(v => v.ItemsSource);
            sFFastLineSeries[6].Color = UIColor.Brown;
            //set.Bind(fastLineSeries)
            //             .To(vm => vm.DataListMobile[DataType.ecg])
            //             .For(v => v.ItemsSource);
            //            set.Bind(graph2Data)
            //                .To(vm => vm.Hello)
            //                .For(v => v)
            //                .OneWay();

            set.Apply();
        }
    }
}
